<?php
/**
 * Class     Data.php
 * @category Bitbull
 * @package  Bitbull_PromotionFlow
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PromotionFlow_Helper_Data extends Mage_Core_Helper_Abstract {

    const ACTIVE_SWITCH_CONFIG_PATH                 = 'promo/bitbull_promotionflow_settings/active';
    const PAYMENT_METHODS_LIST_CONFIG_PATH          = 'promo/bitbull_promotionflow_settings/methods';
    const DEFAULT_ORDER_STATE_STATUS_CONFIG_PATH    = 'promo/bitbull_promotionflow_settings/default_order_statuses';
    const ORDER_STATE_STATUS_EXCEPTIONS_CONFIG_PATH = 'promo/bitbull_promotionflow_settings/exceptions';

    public function isModuleActive()
    {
        return Mage::getStoreConfig(self::ACTIVE_SWITCH_CONFIG_PATH);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function canUpdatePromotions(Mage_Sales_Model_Order $order)
    {
        if (!$this->isModuleActive()) {
            return true;
        }

        $method = $order->getPayment()->getMethod();
        if (!$this->isAffectedPaymentMethod($method)) {
            return true;
        }

        return $this->checkStateStatus($order->getState(), $order->getStatus(), $method);
    }

    public function isCriticalOrderStateChange(Mage_Sales_Model_Order $order)
    {
        if (!$this->isModuleActive()) {
            return false;
        }

        $method = $order->getPayment()->getMethod();
        if (!$this->isAffectedPaymentMethod($method)) {
            return false;
        }

        //Se si passa da uno stato non tra quelli elencati a uno di quelli indicati (o viceversa), allora ritorna true
        return $this->checkStateStatus($order->getOrigData('state'), $order->getOrigData('status'), $method) !==
        $this->checkStateStatus($order->getState(), $order->getStatus(), $method);
    }

    private function isAffectedPaymentMethod($method)
    {
        return in_array($method, explode(',', Mage::getStoreConfig(self::PAYMENT_METHODS_LIST_CONFIG_PATH)));
    }

    private function checkStateStatus($orderState, $orderStatus, $method)
    {
        $stateStatuses = explode(',', Mage::getStoreConfig(self::DEFAULT_ORDER_STATE_STATUS_CONFIG_PATH));

        $exceptions = unserialize(Mage::getStoreConfig(self::ORDER_STATE_STATUS_EXCEPTIONS_CONFIG_PATH));
        if (is_array($exceptions) && array_key_exists($method, $exceptions)) {
            $stateStatuses = $exceptions[$method]['order_statuses'];
        }

        foreach ($stateStatuses as $stateStatus) {
            if (empty($stateStatus)) {
                continue;
            }

            list($state, $status) =
                explode(
                    Bitbull_PromotionFlow_Model_System_Config_Source_Order_StateStatus::STATE_STATUS_SEPARATOR,
                    $stateStatus
                );
            if ($orderState == $state && $orderStatus == $status) {
                return true;
            }
        }

        return false;
    }
} 