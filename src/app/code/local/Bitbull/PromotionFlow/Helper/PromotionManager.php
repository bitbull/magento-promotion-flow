<?php
/**
 * Class     PromotionManager.php
 * @category Bitbull
 * @package  Bitbull_PromotionFlow
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PromotionFlow_Helper_PromotionManager  {


    public function revertPromotionUsageForOrder(Mage_Sales_Model_Order $order)
    {
        $ruleIds = explode(',', $order->getAppliedRuleIds());
        $ruleIds = array_unique($ruleIds);

        $ruleCustomer = null;
        $customerId = $order->getCustomerId();

        if ($order->getDiscountAmount() != 0) {
            foreach ($ruleIds as $ruleId) {
                if (!$ruleId) {
                    continue;
                }
                $rule = Mage::getModel('salesrule/rule');
                $rule->load($ruleId);
                if ($rule->getId()) {
                    $rule->setTimesUsed($rule->getTimesUsed() - 1);
                    $rule->save();

                    if ($customerId) {
                        $ruleCustomer = Mage::getModel('salesrule/rule_customer');
                        $ruleCustomer->loadByCustomerRule($customerId, $ruleId);

                        if ($ruleCustomer->getId()) {
                            $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() - 1);
                        }
                        $ruleCustomer->save();
                    }
                }
            }
            $coupon = Mage::getModel('salesrule/coupon');
            /** @var Mage_SalesRule_Model_Coupon */
            $coupon->load($order->getCouponCode(), 'code');
            if ($coupon->getId()) {
                $coupon->setTimesUsed($coupon->getTimesUsed() - 1);
                $coupon->save();
                if ($customerId) {
                    $couponUsage = Mage::getResourceModel('bitbull_promotionflow/coupon_usage');
                    $couponUsage->revertCustomerCouponTimesUsed($customerId, $coupon->getId());
                }
            }
        }

    }
} 