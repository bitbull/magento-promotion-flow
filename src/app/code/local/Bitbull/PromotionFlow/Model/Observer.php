<?php
 /**
 * Class     Observer.php
 * @category Bitbull
 * @package  Bitbull_PromotionFlow
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PromotionFlow_Model_Observer{

    //consts used to mark promotions of the order as updated, to not elaborate them more then one time
    const  BITBULL_PROMOTIONFLOW_SET_PRODUCT_AS_UPDATED_AS_REVERT ='bitbull_promotionflow_set_product_as_updated_as_revert';
    const  BITBULL_PROMOTIONFLOW_SET_PRODUCT_AS_UPDATED_AS_PLACE ='bitbull_promotionflow_set_product_as_updated_as_place';

    /**
     * @param $observer Varien_Event_Observer
     */
    public function salesOrderPlaceAfter($observer)
    {
        $order = $observer->getEvent()->getOrder();

        /** @var Bitbull_PromotionFlow_Helper_Data $helper */
        $helper = Mage::helper('bitbull_promotionflow');

        if (!$helper->canUpdatePromotions($order)
            && $order->getData(self::BITBULL_PROMOTIONFLOW_SET_PRODUCT_AS_UPDATED_AS_REVERT)==null) {
            //if the promotion can not be update will be revert the usage
            Mage::helper('bitbull_promotionflow/promotionManager')->revertPromotionUsageForOrder($order);
            $order->setData(self::BITBULL_PROMOTIONFLOW_SET_PRODUCT_AS_UPDATED_AS_REVERT, true);

        }
    }

    /**
     * @param $observer Varien_Event_Observer
     */
    public function salesOrderSaveAfter($observer)
    {
        $order = $observer->getEvent()->getOrder();

        /** @var Bitbull_PromotionFlow_Helper_Data $helper */
        $helper = Mage::helper('bitbull_promotionflow');

        if ($helper->isCriticalOrderStateChange($order)
            && $order->getData(self::BITBULL_PROMOTIONFLOW_SET_PRODUCT_AS_UPDATED_AS_PLACE)==null) {

            Mage::getSingleton('salesrule/observer')->sales_order_afterPlace($observer);
            $order->setData(self::BITBULL_PROMOTIONFLOW_SET_PRODUCT_AS_UPDATED_AS_PLACE, true);

        }
    }
    
} 