<?php
/**
 * Class     MultiselectRenderer.php
 * @category Bitbull
 * @package  Bitbull_PromotionFlow
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PromotionFlow_Block_Adminhtml_Form_Field_MultiselectRenderer extends Mage_Core_Block_Html_Select
{
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    public function getName()
    {
        $name = parent::getName();
        if (strpos($name, '[]') === false) {
            $name.= '[]';
        }
        return $name;
    }
}
