<?php
/**
 * Class     PaymentStatusExceptions.php
 * @category Bitbull
 * @package  Bitbull_PromotionFlow
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_PromotionFlow_Block_Adminhtml_Form_Field_PaymentStatusExceptions
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract{

    private $_paymentMethodRenderer;
    private $_orderStatusesRenderer;

    protected function _prepareToRender()
    {
        $this->addColumn(
            'payment_method',
            array(
                'label' => Mage::helper('adminhtml')->__('Payment Method'),
                'renderer' => $this->_getPaymentMethodRenderer(),
            )
        );
        $this->addColumn(
            'order_statuses',
            array(
                'label' => Mage::helper('adminhtml')->__('Order Statuses'),
                'renderer' => $this->_getOrderStatusesRenderer(),
            )
        );
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add Exception');
    }

    private function _getOrderStatusesRenderer()
    {
        if ($this->_orderStatusesRenderer) {
            return $this->_orderStatusesRenderer;
        }

        $this->_orderStatusesRenderer = $this->_createOrderStatusesRenderer();
        return $this->_orderStatusesRenderer;
    }


    private function _createOrderStatusesRenderer()
    {
        $renderer = $this->getLayout()->createBlock(
            'bitbull_promotionflow/adminhtml_form_field_multiselectRenderer', '', array('is_render_to_js_template' => true)
        );
        $options = array_merge(
            array(
                array(
                    'value' => '', 'label' => Mage::helper('adminhtml')->__('-- Please Select --')
                )
            ),
            Mage::getModel('bitbull_promotionflow/system_config_source_order_stateStatus')->toOptionArray()
        );
        $renderer->setOptions($options);
        $renderer->setExtraParams('style="width:200px; height:150px;" multiple="multiple"');
        return $renderer;
    }


    private function _getPaymentMethodRenderer()
    {
        if ($this->_paymentMethodRenderer) {
            return $this->_paymentMethodRenderer;
        }

        $this->_paymentMethodRenderer = $this->getLayout()->createBlock(
            'bitbull_promotionflow/adminhtml_form_field_selectRenderer', '', array('is_render_to_js_template' => true)
        );
        $options = array_merge(
            array(array('value' => '', 'label' => Mage::helper('adminhtml')->__('-- Please Select --'))),
            Mage::getSingleton('adminhtml/system_config_source_payment_allmethods')->toOptionArray()
        );
        $this->_paymentMethodRenderer->setOptions($options);
        $this->_paymentMethodRenderer->setExtraParams('style="width:300px"');
        return $this->_paymentMethodRenderer;
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getPaymentMethodRenderer()->calcOptionHash($row->getData('payment_method')),
            'selected="selected"'
        );
        foreach ($row->getData('order_statuses') as $value) {
            $row->setData(
                'option_extra_attr_' . $this->_getOrderStatusesRenderer()->calcOptionHash(
                    $value
                ),
                'selected="selected"'
            );
        }
    }
}
