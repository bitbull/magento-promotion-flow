Bitbull PromotionFlow Extension
=====================
Module to not mark as used the promotions if the order is not valid
Facts
-----
- version: 1.0.0
- extension key: Bitbull_PromotionFlow

Description
-----------
This module add the possibility to not mark as used the sales rule promotion (coupon and sales rule).

Requirements
------------
- PHP >= 5.2.0
- Mage_Core
- ...

Compatibility
-------------
- Magento >= 1.4

Copyright
---------
(c) 2015 Bitbull
